/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.edgar;

/**
 *
 * @author Edgar Alfaro
 */
public class Pizzeria {
    public static void main(String[] args) {
        // Paso 3: Instanciar objetos
        Pizza pizza1 = new Pizza();
        pizza1.getNombre();
        System.out.println(pizza1.getNombre());
        pizza1.setNombre("Española");
        System.out.println("nombre pizza1: " + pizza1.getNombre());
        
        Pizza pizza2 = new Pizza("pepperoni", "familiar", "normal");
        System.out.println("nombre pizza2: " + pizza2.toString()); 
        pizza2.setNombre("vegetariana");
        System.out.println(pizza2.toString());
        
        //print solo un atributo, print todos los atributos , print metodos
        
        Pizza pizza3 = new Pizza ("todas las carnes", "mediana", "borde queso");
        pizza3.preparar();
        System.out.println("Pizza 3 nombre: " + pizza3.getNombre());
        System.out.println("pizza 3 tamano: " + pizza3.getTamano());
        System.out.println("pizza3 masa: " + pizza3.getMasa());
        System.out.println(pizza3.toString());
        pizza3.calentar();
    }
}
