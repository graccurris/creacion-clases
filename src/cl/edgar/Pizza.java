/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.edgar;

/**
 *
 * @author Edgar alfaro
 */
public class Pizza {
    // Paso 1: crear atributos
    private String nombre;
    private String tamano;
    private String masa;
    
    // Paso 2: construir los metodos (customer)
    public void preparar (){
        System.out.println("Se esta preparando su pizza");
    }
    public void calentar (){
        System.out.println("Se esta horneando su pizza");
    }
    //Paso 4: crear constructor con y sin parametros

    public Pizza() {
    }

    public Pizza(String nombre, String tamano, String masa) {
        this.nombre = nombre;
        this.tamano = tamano;
        this.masa = masa;
    }
    
    //paso 5: crear metodos setter y getter

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTamano() {
        return tamano;
    }

    public void setTamano(String tamano) {
        this.tamano = tamano;
    }

    public String getMasa() {
        return masa;
    }

    public void setMasa(String masa) {
        this.masa = masa;
    }
    
    //paso 6: crear metodo tostring 

    @Override
    public String toString() {
        return "Pizza{" + "nombre=" + nombre + ", tamano=" + tamano + ", masa=" + masa + '}';
    }
    
}
